from ..protocols import collector_pb2
from ..common import sensor_types
from ..common import config

import paho.mqtt.publish as publish
import redis
import json

def main():
    config.init('mqtt')

    mqtt_hostname = config.get('Mqtt', 'Hostname')
    mqtt_port = config.get('Mqtt', 'Port')
    mqtt_auth_token = config.get('Mqtt', 'AuthToken')
    mqtt_client_id = config.get('Mqtt', 'ClientId')

    redis_host = config.get('Redis', 'Host')
    redis_port = config.getint('Redis', 'Port')

    r = redis.StrictRedis(host=redis_host, port=redis_port)
    p = r.pubsub()
    p.psubscribe("sensor_status.*")

    for message in p.listen():
        if message['type'] not in ['message', 'pmessage']:
            print "Ignoring message %s" % message
            continue

        try:
            # TODO: Note that this will attempt to parse any messages as bare SensorStatus objects.
            # Maybe we should define a new wrapper message type with oneof (like a tagged union).
            status = collector_pb2.SensorStatus.FromString(message['data'])
        except Exception as e:
            print "Error parsing redis message, %s" % e
            continue

        try:
            type_id = "python-gateway" #"pi_sensors"
            device_id = "aukena" #"pi2"
            event = "status"
            topic = "iot-2/type/%s/id/%s/evt/%s/fmt/json" % (type_id, device_id, event)
            topic = "iot-2/type/%s/id/%s/evt/%s/fmt/json" % (type_id, device_id, event)
            payload = status_as_mqtt_json(status, message['channel'])

            print "---"

            print mqtt_hostname, mqtt_port
            print mqtt_client_id, mqtt_auth_token
            print topic
            print payload

            

            publish.single(
                topic,
                payload = payload,
                hostname = mqtt_hostname,
                port = mqtt_port,
                client_id = mqtt_client_id,
                auth = {'username': 'use-token-auth', 'password' : mqtt_auth_token}
            )
        except Exception as e:
            print "Error: %s" % e


def status_as_mqtt_json(status, channel):
    sensor_type = sensor_types.sensor_type_name(status.sensor_type) 
    value = round(status.float_value, 2) or status.int_value or status.bool_value;
    return json.dumps({
        'd' : {
            sensor_type: value,
            'hostname': status.hostname,
            'timestamp': status.timestamp
        }
    }, separators=(',', ': '))

if __name__ == '__main__':
    main()

