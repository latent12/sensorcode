from ..protocols import measurements_pb2
from ..protocols import collector_pb2
from ..common import krpc as implementations
from ..common import sensor_types, config

from threading import Thread
from Queue import Queue
import time
import pymongo
import redis

class MeasurementsService(measurements_pb2.MeasurementsService):
    received_cb = None

    def __init__(self):
        measurements_pb2.MeasurementsService.__init__(self)
        self.received_cb = []

    def AddReceivedCallback(self, cb):
        self.received_cb.append(cb)

    def Report(self, rpc_controller, request, context):
        """ Implement the Report handler of the Measurements Service interface. """
        print "Measurements server received a report."

        for cb in self.received_cb:
            # TODO it may be more efficient to do the unpacking here and pass status objects
            # only, which are treated as immutable. We could then have a different callback
            # for end of batch that could trigger the pipeline/batch submissions.

            # Pass a copy to ensure independent iterators are created.
            copy = measurements_pb2.Measurements()
            copy.CopyFrom(request)
            cb(copy)

        # TODO delay ACK until actually in database or journal or something?
        return measurements_pb2.MeasurementsAck(measurements_received = len(request.data))

class MeasurementsSender():
    """ Base class for different measurements sender implementations.

    Implementations must take care to override Send() and call the constructor. """
    q_received = None
    q_send = None

    def __init__(self):
        self.q_received = Queue()
        self.q_send = Queue()

    def Start(self):
        """ Starts the sending and unpacking threads. """
        Thread(target=self.RunSending).start()
        Thread(target=self.RunUnpacking).start()

    def ReceivedCallback(self, measurements):
        """ Callback for when a new measurements message arrives. """
        self.q_received.put(measurements)

    def RunUnpacking(self):
        """ A thread that takes measurements messages and unpacks them,
        putting each status contained therein onto the send queue. """

        def too_new(status):
            """ Checks whether the status is more than two minutes ahead of the server time. """
            # statuses with time stamps too far in the future are likely the result of
            # sensor clock changes and should be discarded.
            return (status.timestamp - time.time()) > 120

        while True:
            m = self.q_received.get()
            for status in m.data:
                status.hostname = m.hostname
                if not status.error and not too_new(status):
                    self.q_send.put((m.hostname, status))
                else:
                    print "Discarded status, error or too new: %s", status

    def RunSending(self):
        """ A thread that calls the implementation specific Send() method
        for each individual sensor status taken from the send queue. """
        while True:
            hostname, status = self.q_send.get()
            try:
                self.Send(hostname, status)
            except Exception as e:
                print "Exception in MeasurementsSender.Send() implementation, %s" % e.message

    def Send(self, hostname, status):
        """Override this with target database specific implementation.
        Sends a single status to the database.
        """
        print "Implementations of MeasurementsSender must override Send()."
        raise NotImplementedError()

class MongoMeasurementsSender(MeasurementsSender):
    db = None

    def __init__(self, mongo_host, mongo_port, mongo_database):
        """ Call super constructor and initialize MongoDB connection. """
        MeasurementsSender.__init__(self)
        self.db = pymongo.MongoClient(mongo_host, mongo_port)[mongo_database]

    def Send(self, hostname, s):
        """ Inserts a sensor status into the MongoDB SensorReadingCollection. """
        # TODO: Batch insert might be more efficient, but as described below,
        # this is not relevant here (yet).
        try:
            # Insert an individual sensor reading into the MongoDB collection.
            t = collector_pb2.SensorStatus
            #if it is an occupancy reading insert in the occupancy collection
            if s.sensor_type == t.OCCUPANCY:
                self.db.RoomOccupancy.insert({
                    'timestamp': s.timestamp,
                    'node_name': hostname,
                    'value': s.bool_value,
                    'predictor_hash': s.hash
                })
            else:
            # if it is a sensor measurement insert in the readings collection 
                self.db.SensorReadingCollection.insert({
                    'timestamp': s.timestamp,
                    'value': s.int_value or s.float_value or s.bool_value,
                    'node_name': hostname,
                    'sensor_type': sensor_types.sensor_type_name(s.sensor_type)
                    })
        except Exception as e:
            print "Failed to send to Mongo, discarding status. %s" % e.message

class RedisMeasurementsSender(MeasurementsSender):
    r = None

    def __init__(self, redis_host, redis_port):
        """ Call super constructor and initialize Redis connection. """
        MeasurementsSender.__init__(self)
        self.r = redis.StrictRedis(host=redis_host, port=redis_port)

    def Send(self, hostname, s):
        """ Publish a Redis event on channel sensor_status.<hostname>.<sensor_type> """
        # TODO: When there are many readings, pipelining the insertions would be more
        # efficient, but to do that, we would need direct access to the queue, or to the
        # original measurements object.
        # This will become irrelevant when the original measurements objects are only passed
        # to a Redis queue and processed separately.
        try:
            channel = 'sensor_status.%s.%s' % (hostname, sensor_types.sensor_type_name(s.sensor_type))
            self.r.publish(channel, s.SerializeToString())
        except Exception as e:
            print "Failed to send to Redis, discarding status. %s" % e.message

def main():
    # Get configuration values
    config.init('measurements')
    port = (config.get('Measurements', 'Host'), config.getint('Measurements', 'Port'))

    mongo_host = config.get('Mongo', 'Host')
    mongo_port = config.getint('Mongo', 'Port')
    mongo_database = config.get('Mongo', 'Database')

    redis_host = config.get('Redis', 'Host')
    redis_port = config.getint('Redis', 'Port')

    # Start Mongo Sender
    mongo_sender = MongoMeasurementsSender(mongo_host, mongo_port, mongo_database)
    mongo_sender.Start()

    # Start Redis Sender
    redis_sender = RedisMeasurementsSender(redis_host, redis_port)
    redis_sender.Start()

    # Initialize servicer and register received callbacks for each of the senders
    servicer = MeasurementsService()
    servicer.AddReceivedCallback(mongo_sender.ReceivedCallback)
    servicer.AddReceivedCallback(redis_sender.ReceivedCallback)

    # Start the TCP server
    server = implementations.create_server(servicer)
    server.add_insecure_port("%s:%d" % port)
    server.serve_forever()

if (__name__ == "__main__"):
    main()
