def temp_config():
   
    i2c_host = config.get('TemperatureSensor', 'I2cHost')
    i2c_port = config.get('I2c', 'Port')

    channel = implementations.insecure_channel(i2c_host, i2c_port)
    self.i2c = i2c_pb2.I2cService_Stub(channel)

    return i2c_host, i2c_port, channel, self.i2c
