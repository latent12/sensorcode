import mqtt
import collector
import time

try:
  client = MQTT.connect_mqtt()
except OSError as e:
  MQTT.restart_and_reconnect()

while True:
  
  last_message = 0
  message_interval = 10
  
  if (time.time() - last_message) > message_interval:
    
    time.sleep(5)
    Collector.collect_sensor()
    time.sleep(5)
    
    last_message = time.time()





























