import redis
import requests
import time

def main():
    config.init('upload')
    redis_host = config.get('Redis', '')
    redis_port = config.getint('Redis', '')
    upload_url = config.get('Upload', '')
    upload_interval = config.getint('Upload', 'Interval')
    last_upload = 0;
    data = dict()

    r = redis.StrictRedis(host=redis_host, port=redis_port)
    p = r.pubsub()
    p.psubscribe("sensor_status.*")

    for message in p.listen():
        if message['type'] not in ['message', 'pmessage']:
            continue

        try:
            status = collector_pb2.SensorStatus.FromString(message['data'])
            hostname = status.hostname
            sensor_type = sensor_types.sensor_type_name(status.sensor_type)

            key = "%s/%s" % (hostname, sensor_type)

            sensor_value = str(round(status.float_value, 2) or status.int_value or status.bool_value)
            data[key] = "%s,%s" % (status.timestamp, sensor_value)
        except Exception as e:
            print (e)
            continue

        if (time.time() - last_upload > upload_interval):

            url = '%s/%s' % (upload_url)

            req = requests.post(url, data=data)
            last_upload = time.time()

            if (req.status_code == 200):
                print ("data uploaded")
            else:
                print ("error uploading (%d)" % req.status_code)
            print (req.text)
            

        if __name__ == '__main__':
          main()


