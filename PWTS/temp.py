from machine import Pin, I2C
import tempconnect
import time

class TemperatureSensor():
  
    _DEVICE_ADDR = 0x45 #memory space to activate SHT30 Sensor
    
    i2c = TempConnect.temp_connect()
    
    mem1 = bytearray(2)
    mem2 = bytearray(6)
    
    #memory spaces accessed by SHT30()
    reg1 = 0x30
    reg2 = 0xA2
    reg3 = 0x2c
    reg4 = 0x06

    def get_sensor_value(self):
      
        results = self.i2c
        addr = self._DEVICE_ADDR
        
        if results != None:
          
           buf = self.mem1
           buf[0] = self.reg1
           buf[1] = self.reg2
           results.writeto(addr, buf)
           time.sleep_ms(1000)
          
           data = self.mem2
           buf[0] = self.reg3
           buf[1] = self.reg4
           results.writeto(addr, buf)
           data = results.readfrom(addr, 6)
           
           data2 = self._swap_word_bytes(data)
           return data2
        
        else:
          
            print ("Problem with response from i2c service: %s" % results)
            return None
            
    def _swap_word_bytes(self, data):

        temp_raw = (data[0] << 8) + (data[1])
        humi_raw = (data[2] << 8) + (data[3])
        temp = 175 * temp_raw / 65535 - 45 #Formula Taken From MicroPython SHT30() Driver
        humi = 100 * humi_raw / 65535 #Formula Taken From MicroPython SHT30() Driver
        
        #return "humi = {:.2f}".format(humi)
        
        #return "temp = {:.2f} humi = {:.2f}".format(temp, humi)
        
        return ("temp = {:.2f} humi = {:.2f}".format(temp, humi))
  
#Activate the line of code to test sensor individually 
   
while True:
  time.sleep(5)
  test = TemperatureSensor()
  print (test.get_sensor_value())
  time.sleep(5)













