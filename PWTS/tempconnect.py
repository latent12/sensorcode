class TempConnect:
  
  @staticmethod
  def temp_connect():
   
      i2c = I2C(scl=Pin(5), sda=Pin(4), freq=100000)
      return i2c
